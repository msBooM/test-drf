from django.db import models


class List(models.Model):
    id = models.BigAutoField(primary_key=True)
    name = models.CharField(max_length=100)


class Item(models.Model):
    id = models.BigAutoField(primary_key=True)
    list = models.ForeignKey(List, related_name='items', on_delete=models.CASCADE)
    name = models.CharField(max_length=100)
