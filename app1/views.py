from rest_framework import routers, serializers, viewsets

from . import models


class ItemModelSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Item
        fields = ['id', 'list', 'name']


class ListModelSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.List
        fields = ['id', 'name']


class ListModelViewSet(viewsets.ModelViewSet):
    queryset = models.List.objects.all()
    serializer_class = ListModelSerializer


class ItemModelViewSet(viewsets.ModelViewSet):
    queryset = models.Item.objects.all()
    serializer_class = ItemModelSerializer

