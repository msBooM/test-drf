# Example

https://httpie.io/


1. Create list
```bash
$ http POST localhost:8000/api/v1/lists/ name=list1
HTTP/1.1 201 Created
Allow: GET, POST, HEAD, OPTIONS
Content-Length: 23
Content-Type: application/json
Date: Wed, 10 Nov 2021 05:28:15 GMT
Referrer-Policy: same-origin
Server: WSGIServer/0.2 CPython/3.9.7
Vary: Accept, Cookie
X-Content-Type-Options: nosniff
X-Frame-Options: DENY

{
    "id": 1,
    "name": "list1"
}
```

2. View lists
```bash
$ http GET localhost:8000/api/v1/lists/
HTTP/1.1 200 OK
Allow: GET, POST, HEAD, OPTIONS
Content-Length: 25
Content-Type: application/json
Date: Wed, 10 Nov 2021 05:28:44 GMT
Referrer-Policy: same-origin
Server: WSGIServer/0.2 CPython/3.9.7
Vary: Accept, Cookie
X-Content-Type-Options: nosniff
X-Frame-Options: DENY

[
    {
        "id": 1,
        "name": "list1"
    }
]
```

3. Create item
```bash
$ http POST localhost:8000/api/v1/items/ name=item1 list=1
HTTP/1.1 201 Created
Allow: GET, POST, HEAD, OPTIONS
Content-Length: 32
Content-Type: application/json
Date: Wed, 10 Nov 2021 05:29:25 GMT
Referrer-Policy: same-origin
Server: WSGIServer/0.2 CPython/3.9.7
Vary: Accept, Cookie
X-Content-Type-Options: nosniff
X-Frame-Options: DENY

{
    "id": 1,
    "list": 1,
    "name": "item1"
}


$ http POST localhost:8000/api/v1/items/ name=item2 list=1
HTTP/1.1 201 Created
Allow: GET, POST, HEAD, OPTIONS
Content-Length: 32
Content-Type: application/json
Date: Wed, 10 Nov 2021 05:29:31 GMT
Referrer-Policy: same-origin
Server: WSGIServer/0.2 CPython/3.9.7
Vary: Accept, Cookie
X-Content-Type-Options: nosniff
X-Frame-Options: DENY

{
    "id": 2,
    "list": 1,
    "name": "item2"
}
```

4. View items
```bash
$ http GET localhost:8000/api/v1/items/
HTTP/1.1 200 OK
Allow: GET, POST, HEAD, OPTIONS
Content-Length: 67
Content-Type: application/json
Date: Wed, 10 Nov 2021 05:29:59 GMT
Referrer-Policy: same-origin
Server: WSGIServer/0.2 CPython/3.9.7
Vary: Accept, Cookie
X-Content-Type-Options: nosniff
X-Frame-Options: DENY

[
    {
        "id": 1,
        "list": 1,
        "name": "item1"
    },
    {
        "id": 2,
        "list": 1,
        "name": "item2"
    }
]
```
